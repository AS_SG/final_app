package group7.egg.eventos.entidades;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class Transporte {
    
    @Id
    //Con lo siguiente hacemos que se asigne un Id automáticamente que no se repetirá nunca
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String tipo;
    private String patente;
    private int lugaresDisponibles;
    private int precioPorPersona;
    private String lugarDeSalida;
      
    
    
    @OneToOne //DESPUÉS DE CREAR LA ENTIDAD FOTO, MARCAMOS LA RELACIÓN ENTRE TRANSPORTE Y FOTO CON @ONETOONE (CADA TRANSPORTE TIENE SÓLO UNA FOTO)
    private Foto foto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public int getLugaresDisponibles() {
        return lugaresDisponibles;
    }

    public void setLugaresDisponibles(int lugaresDisponibles) {
        this.lugaresDisponibles = lugaresDisponibles;
    }

    public int getPrecioPorPersona() {
        return precioPorPersona;
    }

    public void setPrecioPorPersona(int precioPorPersona) {
        this.precioPorPersona = precioPorPersona;
    }

    public String getLugarDeSalida() {
        return lugarDeSalida;
    }

    public void setLugarDeSalida(String lugarDeSalida) {
        this.lugarDeSalida = lugarDeSalida;
    }
    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }

    
    
    
    
    
    
    
}
