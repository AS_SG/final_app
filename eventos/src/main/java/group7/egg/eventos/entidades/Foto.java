package group7.egg.eventos.entidades;

//import static javafx.scene.input.KeyCode.F;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class Foto {
    
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    
    private String nombre;
    private String mime; //MIME ASIGNA EL FORMATO DEL ARCHIVO DE LA FOTO, QUÉ TIPO DE FOTO VAMOS A UTILIZAR PARA DEVOLVERLE AL NAVEGADOR CUANDO NOS LA PIDA
    
    @Lob @Basic(fetch = FetchType.LAZY)//@LOB IDENTIFICA QUE EL TIPO DE DATO ES PESADO / @Basic(fetch = FetchType.LAZY) INDICA QUE EL CONTENIDO CARGUE SÓLO CUANDO LO PIDAMOS, HACIENDO QUE LOS QUERIES SEAN MÁS LIVIANOS 
    //ENTONCES, CUANDO CONSULTE POR LA FOTO, SÓLO TRAERÁ LOS OTROS ATRIBUTOS Y CON UN GET TRAIGO LOS ATRIBUTOS LAZY
    private byte[] contenido;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public byte[] getContenido() {
        return contenido;
    }

    public void setContenido(byte[] contenido) {
        this.contenido = contenido;
    }
    
    
    
    
    
    
}
