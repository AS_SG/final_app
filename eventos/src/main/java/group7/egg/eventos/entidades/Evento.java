package group7.egg.eventos.entidades;

import group7.egg.eventos.enumeraciones.TipoDeEvento;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class Evento {
    
    
    @Id
    //Con lo siguiente hacemos que se asigne un Id automáticamente que no se repetirá nunca
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String nombre;
    private Date fecha;
    private String descripcion;
    private Time hora;
    
    @Enumerated(EnumType.STRING)
    private TipoDeEvento tipoDeEvento;
    
    @OneToOne //DESPUÉS DE CREAR LA ENTIDAD FOTO, MARCAMOS LA RELACIÓN ENTRE EVENTO Y FOTO CON @ONETOONE (CADA EVENTO TIENE SÓLO UNA FOTO)
    private Foto foto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public TipoDeEvento getTipoDeEvento() {
        return tipoDeEvento;
    }

    public void setTipoDeEvento(TipoDeEvento tipoDeEvento) {
        this.tipoDeEvento = tipoDeEvento;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }
    
    
    
    
    
    
    
}
