package group7.egg.eventos.controladores;

import group7.egg.eventos.entidades.Usuario;
import group7.egg.eventos.services.UsuarioService;
import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class UsuarioControlador {
    
   @Autowired
   private UsuarioService usuarioService;
   
   @Autowired
   private ErrorServicio errorServicio;
   
   @Autowired
   private NotificacionServicio notificacionServicio;
   
   @Transactional
   public void registrar (String nombre, String apellido, String mail, String clave) throws ErrorServicio {
     
       Validar (nombre, apellido, mail, clave);
       
       Usuario usuario = new Usuario;
       usuario.setNombre(nombre);
       usuario.setApellido(apellido);
       usuario.setMail(mail);
       
       String encriptada = new BCryptPasswordEncoder().encode(clave);
       usuario.setCalve(encriptada);
       usuario.setAlta(new Date);
       
       usuarioService.save(usuario);
                           
       @Transactional
       public void Modificar (String nombre, String apellido, String mail, String clave) throws ErrorServicio {
       
       Validar (nombre, apellido, mail, clave);
       
       Optional<Usuario> respuesta = usuarioService.findById();
       if (respuesta.isPresent()){
              Usuario usuario = respuesta.get();
              usuario.setNombre(nombre);
              usuario.setApellido(apellido);
              usuario.setMail(mail);
              
              String encriptada = new BCriptPasswordEncoder().encode(clave);
              usuario.setClave(encriptada);
       
   }
       
   }
       
       @Transactional
       public void deshabilitar (String id) throws ErrorServicio {
       
       Optional<Usuario> respuesta = usuarioService.findById(id);
         if (respuesta.isPresent()) {
                 Usuario usuario = respuesta.get();
                 usuario.setBaja(null);
                 usuarioService.save(usuario);
                 
         }else {
             throws new ErrorServicio("No existe el Usuario solicitado");
         }
   }
       
       @Transactional
       public void habilitar (String id) throws ErrorServicicio {
       
       Optional<Usuario> respuesta = usuarioService.findById(id);
       if (respuesta.isPresent()) {
                Usuario usuario = respuesta.get();
                usuario.setBaja(null);
                usuarioService.save(usuario);
       }else {
           throws new ErrorServicio("No existe el Usuario solicitado");
           
       }
               
       
   }
       
       public void Validar (String Nombre, String Apellido, String mail, String Clave) throws ErrorServicio {
           
           if (nombre == null || nombre.isEmpty()) {
               throws new ErrorServicio("El nombre no puede ser nulo");
            }
           if (apellido == null || apellido.isEmpty()) {
               throws new ErrorServicio("El apellido no puede ser nulo");
           }
           if (mail == null || mail.isEmpty())  {
               throws new ErrorServicio("El mail no puede ser nulo");
           }
           if (clave == null || clave.isEmpty() || clave.length() <=6 {
               throws new ErrorServicio("La clave no puede ser nula y debe tener mas de 6 digitos");
           }
                        
       }
       
       

       
       
       
       
   }
   
}
