
package group7.egg.eventos.repositorios;

import group7.egg.eventos.entidades.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventoRepositorio extends JpaRepository<Evento, String> {
    
    
   
    
}
