
package group7.egg.eventos.repositorios;

import group7.egg.eventos.entidades.Transporte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransporteRepositorio extends JpaRepository<Transporte, String> {
    
}
